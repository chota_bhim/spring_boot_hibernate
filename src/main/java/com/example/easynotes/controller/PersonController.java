package com.example.easynotes.controller;


import com.example.easynotes.model.Person;
import com.example.easynotes.repository.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PersonController {
    @Autowired
    PersonRepository personRepository;

    //Get All Notes
    @GetMapping("/person")
    public List<Person> getAllNotes()
    {
        return personRepository.findAll();
    }

}

